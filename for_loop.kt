
class DateRange(val start: MyDate, val end: MyDate) : Iterable<MyDate>{
    override operator fun iterator() = object : Iterator<MyDate>{
        var current : MyDate = start
        override fun next() : MyDate{
            if (!hasNext()) throw NoSuchElementException()
            val next : MyDate = current
            current = current.followingDate()
			return next
        }
       override fun hasNext() : Boolean = current <= end
        
    }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit)  {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}

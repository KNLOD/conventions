import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)
data class TimeIntervalTimes(val timeInterval : TimeInterval, val times : Int)

// Supported intervals that might be added to dates:
enum class TimeInterval { DAY, WEEK, YEAR }

operator fun MyDate.plus(timeInterval: TimeInterval): MyDate{
    return this.addTimeIntervals(timeInterval, amount = 1 )
}

operator fun TimeInterval.times(amount : Int) : TimeIntervalTimes{
    return TimeIntervalTimes(this, amount)
}

operator fun MyDate.plus(timeIntervalTimes : TimeIntervalTimes) : MyDate {
    return this.addTimeIntervals(timeIntervalTimes.timeInterval, timeIntervalTimes.times)
}



fun task1(today: MyDate): MyDate {
    return today + YEAR + WEEK
}

fun task2(today: MyDate): MyDate {
    return today + YEAR * 2 + WEEK * 3 + DAY * 5
}

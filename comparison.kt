data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override operator fun compareTo(date2 : MyDate) : Int{
        return (this.year - date2.year + this.month - date2.month + this.dayOfMonth
                - date2.dayOfMonth)
        
    }
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}
